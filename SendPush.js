import React from 'react';
import moment from 'moment';
import styled from 'react-emotion';
import { TweenMax } from 'gsap/all';
import { Transition } from 'react-transition-group';

import withPaginate from '@/HOC/paginate';
import Table from '@/components/Table/index';
import { store } from '@/actions/historyPush';
import { isEmptyStr } from '@/helpers/global';
import Button from '@/components/Button/index';
import Skeleton from '@/components/Skeleton/index';
import TableRow from '@/components/Table/TableRow';
import TableHead from '@/components/Table/TableHead';
import TableColumn from '@/components/Table/TableColumn';
import { HISTORY_PUSH_FETCH } from '@/constants/actionsTypes';
import { LocationSearchBox, TextInput } from '@/components/Inputs';

class SendPush extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			address: '',
			message: '',
			errors: {},
			lat: null,
			lng: null,
		};
	}
	handleLocation = place => {
		console.log(place);
		const getCityCountry = address_components => {
			if (address_components) {
				let city = '';
				let country = '';
				for (let i = 0; i < address_components.length; i++) {
					const addressType = address_components[i].types[0];
					if (addressType === 'locality') {
						city = address_components[i].long_name;
					}
					if (addressType === 'country') {
						country = address_components[i].long_name;
					}
				}
				return { city, country };
			}
		};
		const NewLocation = {
			address: place.formatted_address,
			lat: place.geometry.location.lat(),
			lng: place.geometry.location.lng(),
			...getCityCountry(place.address_components),
		};
		this.setState(state => ({
			...state,
			...NewLocation,
		}));
	};
	handleLocationAddress = value => {
		let { errors } = this.state;
		delete errors['address'];
		this.setState({
			address: value,
		});
	};

	render() {
		const { address, message, errors } = this.state;
		const { loading, data: pushes } = this.props;

		return (
			<Transition
				timeout={1000}
				mountOnEnter
				unmountOnExit
				appear
				in
				addEndListener={(node, done) => {
					TweenMax.to(node, 1, {
						autoAlpha: 'init' ? 1 : 0,
						onComplete: done,
					});
				}}
			>
				<Wrapper>
					<Content>
						<Form onSubmit={this.storeHistory}>
							<LocationSearchBox
								errorText={'Choose from auto complete'}
								error={errors.address}
								value={address}
								loading={loading}
								name="address"
								label="Location"
								placeholder={'Location'}
								inputOnChange={this.handleLocationAddress}
								onSelectLocation={this.handleLocation}
							/>
							<TextInput
								name="message"
								value={message}
								placeholder="Message"
								label="Message"
								error={errors.message}
								errorText={'Required'}
								onChange={this.handleChangeTextField}
								max={110}
							/>
							<Button disabled={loading} variant="add">
								Send
							</Button>
						</Form>
						<Title>History</Title>
						<Table cellSpacing={0}>
							<thead>
								<TableRow>
									<TableHead width={'200px'}>Date</TableHead>
									<TableHead width={'300px'}>
										Location
									</TableHead>
									<TableHead>Message</TableHead>
								</TableRow>
							</thead>
							<tbody>
								{loading ? (
									<Loader />
								) : (
									pushes.map((push, index) => (
										<TableRow key={index}>
											<TableColumn>
												{moment(push.createdAt).format(
													'DD MMM, YYYY / HH:mm',
												)}
											</TableColumn>
											<TableColumn>
												{push.address}
											</TableColumn>
											<TableColumn>
												{push.message}
											</TableColumn>
										</TableRow>
									))
								)}
							</tbody>
						</Table>
					</Content>
				</Wrapper>
			</Transition>
		);
	}

	storeHistory = async e => {
		e.preventDefault();

		const { message, address, lat, lng } = this.state;
		const validate = data => {
			let error = {};
			Object.keys(data).map(el => {
				error[el] = false;
			});
			if (data) {
				console.log(lat, lng);
				error.address = !data.lat && !data.lng;
				error.message = isEmptyStr(data.message);
				return error;
			}
			return false;
		};
		this.setState({ errors: validate({ message, address, lat, lng }) });
		if (
			!Object.values(validate({ message, address, lat, lng })).some(
				b => b,
			)
		) {
			store({
				message,
				address,
				lat,
				lng,
			})
				.then(() => {
					this.setState({
						message: '',
						address: '',
						errors: {},
						lat: null,
						lng: null,
					});
					this.props.onUpdateData();
				})
				.catch(e =>
					this.setState({
						errors: { ...this.state.errors, ...e.response.data },
					}),
				);
		}
	};

	handleChangeTextField = e => {
		let { errors } = this.state;
		const { name, value } = e.target;
		delete errors[name];
		this.setState({ [name]: value, errors });
	};
}

function mapStateToProps({ historyPush: { pushes, errors, loading, count } }) {
	return {
		data: pushes,
		count,
		errors,
		loading,
	};
}

export default withPaginate(HISTORY_PUSH_FETCH, mapStateToProps)(SendPush);

const Loader = () => {
	return Array.from({ length: 3 }, (v, k) => (
		<tr key={k}>
			<td colSpan={3}>
				<Skeleton height="75px" />
			</td>
		</tr>
	));
};

const Wrapper = styled('div')`
	opacity: 0;
`;

const Content = styled('div')`
	width: 100%;
	max-width: 1260px;
	padding: 0 10px;
	margin: 0 auto;
`;

const Title = styled('h1')`
	font-family: Montserrat;
	font-size: 24px;
	font-weight: 500;
	font-style: normal;
	font-stretch: normal;
	line-height: normal;
	letter-spacing: normal;
	color: #1b2437;
`;

const Form = styled('form')`
	margin-top: 20px;
	display: flex;
	align-items: center;
	justify-content: space-between;
	& > * {
		flex: 1;
		max-width: 513px;
	}
	& > button {
		max-width: 135px;
		min-width: 135px;
		justify-content: center;
	}
	@media (max-width: 600px) {
		flex-direction: column;
	}
`;
