import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Paginate from '@/components/Paginate';

const PAGINATION_CHANGE_TYPES = {
	PAGE: 1,
	LIMIT: 2,
};

const paginate = (action, mapStateToProps, defaultParams = {}) => Component => {
	class PaginateHOC extends React.Component {
		state = {
			page: 1,
			limit: 0,
			next: '',
			previous: '',
			params: defaultParams,
		};

		componentWillMount() {
			this.handleChangePaginate(PAGINATION_CHANGE_TYPES.LIMIT, 10);
		}

		render() {
			const { limit, page } = this.state;
			const { count, loading, data } = this.props;

			return (
				<Fragment>
					<Component
						{...this.props}
						loading={loading}
						data={data}
						onUpdateData={this.refreshData}
						onUpdateParams={this.updateParams}
						history={this.props.history}
					/>
					<Paginate
						limit={limit}
						count={count}
						page={page}
						onChangePage={this.handleChangePaginate.bind(
							this,
							PAGINATION_CHANGE_TYPES.PAGE,
						)}
						onChangeLimit={this.handleChangePaginate.bind(
							this,
							PAGINATION_CHANGE_TYPES.LIMIT,
						)}
					/>
				</Fragment>
			);
		}

		getData = (limit, offset) => {
			return this.props.dispatch({
				type: action,
				params: {
					limit,
					offset: offset === 1 ? 0 : offset,
					...this.state.params,
				},
			});
		};

		refreshData = async () => {
			let { limit, page } = this.state;

			this.getData(limit, (page - 1) * limit);
		};

		async handleChangePaginate(type, value) {
			let { limit, page } = this.state;
			this.setState({ isLoading: true });

			switch (type) {
				case PAGINATION_CHANGE_TYPES.LIMIT:
					limit = value;
					page = 1;
					break;
				case PAGINATION_CHANGE_TYPES.PAGE:
					page = value;
					break;
			}

			this.getData(limit, (page - 1) * limit);

			this.setState({
				limit,
				page,
			});
		}

		updateParams = async (name, value) => {
			const { params, limit } = this.state;
			this.setState(
				{
					page: 1,
					params: { ...params, [name]: value },
				},
				() => this.getData(limit, 1),
			);
		};
	}

	return withRouter(connect(mapStateToProps)(PaginateHOC));
};

export default paginate;
